fx_version 'cerulean'
game 'gta5'

author 'twoot'
description 'Simple admin commands'
version '1.0.0'

client_scripts {
    'helper.lua',
    'commands/*.lua'
}
server_script 'server.lua'