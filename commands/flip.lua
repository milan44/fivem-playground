RegisterCommand('flip', function(source, args)
    local vehicle = GetVehiclePedIsIn(GetPlayerPed(-1), false)
    if vehicle ~= nil then
        local coords = GetEntityCoords(vehicle)
        SetEntityCoords(vehicle, coords.x, coords.y, coords.z+1.0, 0, 0, 0, false)
    end
end, false)