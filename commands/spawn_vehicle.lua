RegisterCommand('sv', function(source, args)
    if not args[1] then
        TriggerEvent('chat:addMessage', {
            args = { 'You need to specify a model name!' }
        })
        return
    end

    local vehicleName = args[1]

    RequestModel(vehicleName)

    while not HasModelLoaded(vehicleName) do
        Wait(500)
    end

    local playerPed = PlayerPedId()
    local pos = GetEntityCoords(playerPed)

    local vehicle = CreateVehicle(vehicleName, pos.x, pos.y, pos.z, GetEntityHeading(playerPed), true, false)

    SetPedIntoVehicle(playerPed, vehicle, -1)

    SetEntityAsNoLongerNeeded(vehicle)

    SetModelAsNoLongerNeeded(vehicleName)
end, false)