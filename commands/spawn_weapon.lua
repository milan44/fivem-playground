RegisterCommand('sw', function(source, args)
    if not args[1] then
        TriggerEvent('chat:addMessage', {
            args = { 'You need to specify a weapon name!' }
        })
        return
    end

    local weapon = "WEAPON_" .. args[1]

    GiveWeaponToPed(PlayerPedId(), weapon:upper(), 1000, false, true)
end, false)