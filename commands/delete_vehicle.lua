RegisterCommand("dv", function(source, args)
    if args[1] == "all" then
        for vehicle in EnumerateVehicles() do
            DeleteEntity(vehicle)
        end
    else
        local player = GetPlayerPed(-1)
        local coords = GetEntityCoords(player)

        local min = 1000.0
        local minVehicle = nil
        for vehicle in EnumerateVehicles() do
            local vCoords = GetEntityCoords(vehicle)
            local distance = GetDistanceBetweenCoords(coords.x, coords.y, coords.z, vCoords.x, vCoords.y, vCoords.z, true)
            if distance < min then
                min = distance
                minVehicle = vehicle
            end
        end

        if minVehicle ~= nil and min < 10 then
            DeleteEntity(minVehicle)
        end
    end
end, false)