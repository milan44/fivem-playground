local invincible = false

RegisterCommand('ind', function(source, args)
    local player = GetPlayerPed(-1)

    if invincible then
        SetEntityInvincible(player, false)
        invincible = false

        TriggerEvent('chat:addMessage', {
            args = { 'Invincibility turned off' }
        })
    else
        SetEntityInvincible(player, true)
        invincible = true

        TriggerEvent('chat:addMessage', {
            args = { 'Invincibility turned on' }
        })
    end
end, false)