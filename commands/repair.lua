RegisterCommand('repair', function(source, args)
    local vehicle = GetVehiclePedIsIn(GetPlayerPed(-1), false)
    if vehicle ~= nil then
        SetVehicleEngineHealth(vehicle, 1000)
        SetVehicleFixed(vehicle)
        SetVehicleDoorsShut(vehicle, true)
        SetVehicleDirtLevel(vehicle, 15)
    end
end, false)