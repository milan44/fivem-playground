local height = function(player)
    local h = GetEntityHeightAboveGround(player)
    print(h)
    return h
end

RegisterCommand('tp', function(source, args)
    local blipid = 8
    local blip = GetFirstBlipInfoId(blipid)
    local player = GetPlayerPed(-1)
    
    if DoesBlipExist(blip) then
        local coords = GetBlipCoords(blip)

        DoScreenFadeOut(250)
        while not IsScreenFadedOut() do
            Citizen.Wait(10)
        end

        StartPlayerTeleport(PlayerId(), coords.x, coords.y, 300.0, GetEntityHeading(player), true, true, true)

        while IsPlayerTeleportActive() do
            Citizen.Wait(10)
        end
        FreezeEntityPosition(player, true)

        local _, z = GetGroundZFor_3dCoord(coords.x, coords.y, 300.0, false)
        
        SetEntityCoords(player, coords.x, coords.y, z + 1.0, true, false, false, false)

        FreezeEntityPosition(player, false)

        DoScreenFadeIn(250)
    else
        TriggerEvent('chat:addMessage', {
            args = { 'You need to specify a waypoint!' }
        })
    end
end, false)